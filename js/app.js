import Vue from "vue";
import _ from "lodash";
import axios from "axios";
import moment from "moment";
import { MDCTopAppBar } from "@material/top-app-bar";
import swal from "sweetalert";
import Cookies from "js-cookie";
// Import the component

var app = new Vue({
  el: "#app",
  data: {
    moment: moment,
    tasks: [],
    loading: true,
  },
  methods: {
    fetch: function () {
      setTimeout(() => {
        axios
          // http://127.0.0.1/tasks.json
          // http://127.0.0.1:5000/tasks/fetch?details=true
          .get(`${Cookies.get("server")}/tasks/fetch?details=true`, {
            withCredentials: true,
          })
          .then((response) => {
            this.loading = false;
            this.tasks = response.data.tasks;
            for (let task = 0; task < this.tasks.length; task++) {
              const element = this.tasks[task];
              element.end_date = moment(element.end_date, "DD.MM.YYYY HH:mm");
            }
            this.tasks.sort(function (a, b) {
              // Turn your strings into dates, and then subtract them
              // to get a value that is either negative, positive, or zero.
              return a.end_date - b.end_date;
            });
          })
          .catch((error) => {
            this.settings();
          });
      }, 3000);
    },
    open_external: function (id) {
      window.open(`https://gym-walsrode.de/iserv/exercise/show/${id}`);
    },
    settings: function (id) {
      swal(`Es wird jetzt nach deinem Iserv passwort und Benutzernamen gefragt. Diese Daten werden verschlüsselt an einen
      deutschen Server übermittelt. Dieser Server loggt sich mit den Zugangsdaten in Iserv ein und führt die angefragte Operation
      durch, d.h. die Aufgaben werden geladen. Die Daten werden nur für die Länge der Bearbeitung gespeichert, nach
      einer Anfrage werden diese Daten wieder gelöscht.`).then(() => {
        swal("Gib den User API Server ein:", {
          content: { element: "input", attributes: {
            placeholder: "https://example.com",
            type: "url",
          } },
        }).then((value) => {
          if (value.slice(-1) === "/") {
            value = value.slice(0,-1)
          }
          if (value.slice(0,8) != "https://" ) {
            value = "https://" + value
          }
          Cookies.set("server", value, { sameSite: 'lax' });
          swal(`Gib den Iserv Username ein:`, {
            content: {
              element: "input",
              attributes: {
                placeholder: "Gib deinen Username ein",
                type: "text",
              },
            },
          }).then((value) => {
            Cookies.set("user", value, { sameSite: 'lax' });
            swal(`Enter your Iserv password for ${value}:`, {
              content: {
                element: "input",
                attributes: {
                  placeholder: "Gib dein Passwort ein",
                  type: "password",
                },
              },
            }).then((value) => {
              Cookies.set("password", value, { sameSite: 'lax' });
              this.fetch();
              swal("Super! Du bist fertig");
            });
          });
        });
      });
    },
    infomessage() {
      swal({
        title: "About",
        text: `I am Alwin Lohrie aka Niwla23 and I am ${calculateAge(
          new Date(2006, 5, 23)
        )} years old.
        On this Page you will find some of my Software Projects.`,
        icon: "info",
      });
    },
  },
  created() {
    const topAppBarElement = document.querySelector(".mdc-top-app-bar");
    new MDCTopAppBar(topAppBarElement);
    this.fetch();
    moment.locale("de");
  },
});
